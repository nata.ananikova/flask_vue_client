import {createRouter, createWebHistory} from "vue-router";
import ProductsitesPage from "@/pages/ProductsitesPage";
import ProductsiteIdPage from "@/pages/ProductsiteIdPage";
import ProductitemIdPage from "@/pages/ProductitemIdPage";


const routes = [
    {
        path: '/',
        component: ProductsitesPage
    },
    {
        path: '/productsites/:productsite_name/:productsite_id',
        component: ProductsiteIdPage,
        props: true
    },
    {
        path: '/productitems/:productitem_name/:productitem_id/:productitem_batch/:productitem_shift/:productitem_date',
        component: ProductitemIdPage,
        props: true
    },

]

const router = createRouter({
    routes,
    history: createWebHistory(process.env.BASE_URL)
})

export default router;
